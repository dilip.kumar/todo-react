import React, { Component } from 'react';
import TodoItem from './TodoItem';

class Todos extends Component {
    deleteTodo(id) {
        this.props.onDelete(id);
    }
    checkChecked(checkedVal, id) {
        this.props.onChecked(checkedVal, id);
    }
    render() {
        let todosItem;
        if (this.props.todos) {
            todosItem = this.props.todos.map(todo => {
                let clsName = todo.chk ? "checked-todo" : "unchecked-todo";
                return (
                    <TodoItem onDelete={this.deleteTodo.bind(this)}
                        onChecked={this.checkChecked.bind(this)} cls={clsName}
                        key={todo.title} todo={todo} />
                );
            })
        }
        return (
            <div className="todos-list">
                {todosItem}
            </div>
        )
    }
}

export default Todos;