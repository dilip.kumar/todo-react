import React, { Component } from 'react';
import TodoItem from './TodoItem';
import uuid from 'uuid';

class AddTodo extends Component {
    constructor() {
        super();
        this.state = {
            newTodo: {}
        }
    }
    handleSubmit(event) {
        if (this.refs.title.value === '') {
            alert("Todo is required");
        } else {
            this.setState({
                newTodo: {
                    id: uuid.v4(),
                    title: this.refs.title.value,
                    chk: false
                }
            }, function () {
                this.props.addTodo(this.state.newTodo);
            })
            this.refs.title.value = ''
        }
        event.preventDefault();
    }

    render() {
        return (
            <div className="main-container">
                <h3>Todo-List</h3>
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <div className="text-field-container">
                        <input type="text" placeholder="Enter your todo here...." ref="title" />
                        <input type="submit" value="Submit" />
                    </div>
                </form>
            </div>
        )
    }
}

export default AddTodo;