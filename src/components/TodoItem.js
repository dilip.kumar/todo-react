import React, { Component } from 'react';

class TodoItem extends Component {
    deleteTodo(id) {
        this.props.onDelete(id);
    }
    checkChecked(event){
        let checkedVal = event.target.checked;
        let id = this.props.todo.id;
        this.props.onChecked(checkedVal,id);
    }
    render() {
        return (
            <div className="todo-item">
                <input type="checkbox" onChange={this.checkChecked.bind(this)}/>
                <strong className ={this.props.cls}> {this.props.todo.title} </strong> 
                <a className="delete-button" href="#" onClick={this.deleteTodo.bind(this, this.props.todo.id)}>X</a>
            </div>
        )
    }
}

export default TodoItem