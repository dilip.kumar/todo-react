import React,{Component} from 'react';
import {render} from 'react-dom';
import '../css/style.css';
import uuid from 'uuid';
import Todos from "../components/Todos";
import AddTodo from "../components/AddTodo";
import natureImage from '../assets/images.jpeg';

class ToDoList extends Component {
    constructor() {
        super();
        this.state = {
            allTodos: []
        }
    }

    handleAddTodo(todo) {
        let todos = this.state.allTodos;
        todos.push(todo);
        this.setState({ allTodos: todos })
    }
    handleDeleteTodo(id) {
        let todos = this.state.allTodos;
        let index = todos.findIndex(x => x.id === id);
        todos.splice(index, 1);
        this.setState({ allTodos: todos })
    }
    handleCheckTodo(checkedVal,id){
        let todos = this.state.allTodos;
        let index = todos.findIndex(x => x.id === id);
        todos[index].chk = checkedVal;
        this.setState({ allTodos: todos })
    }
    render() {
        return (
            <div>
                <AddTodo addTodo={this.handleAddTodo.bind(this)} />
                <Todos todos={this.state.allTodos}  onChecked={this.handleCheckTodo.bind(this)} onDelete={this.handleDeleteTodo.bind(this)} />
            </div>
        )
    }
}
export default ToDoList